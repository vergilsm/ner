namespace :stargazers_nicknames do
  desc "take all nicknames from the stargazers"
  task take_nicknames_from_stargazers: :environment do
    require 'open-uri'
    ruby "app/services/uri.rb"
    ruby "app/services/control.rb"
    require 'slack-notifier'

    logger = Control.logger
    notifier = Control.notifier

    parse_reason = ParseReason.find_by(title: "liked rails")
    base_uri = Uri.base_uri(parse_reason.title)
    user_agent = Uri.user_agent

    if Link.where(parse_reason_id: parse_reason.id).exists?
      base_uri = Link.where(parse_reason_id: parse_reason.id).last.uri
    else
      base_uri
    end

    $i = 0
    $num = 451

    while $i < $num do
      $i += 1
      tries = 0
      tries_nick_doc = 0

      unless $i == $num
        begin
          doc = Uri.doc(base_uri, user_agent)
        rescue => err
          logger.info
          logger.fatal("Caught exception; exiting")
          logger.fatal(err)
          tries += 1
          sleep 350
          if tries <= 2
            notifier.ping "Caught exception; exiting #{err}"
            retry
          end
        end

        link_name = Uri.link_name(doc)
        next_page = Uri.next_page(doc)
        next_page2 = Uri.next_page2(doc)
        button_status = Uri.button_status(doc)
        stargazers_on_page = Uri.stargazers_on_page(doc)

        stargazers_on_page.each do |nick|
          nickname = nick.delete("/\/")
          nick_uri = Uri.nick_uri(nickname)
          begin
            nick_doc = Uri.nick_doc(nick_uri, user_agent)
          rescue => err
            logger.info
            logger.fatal("Caught exception; exiting")
            logger.fatal(err)
            tries_nick_doc += 1
            sleep 350
            if tries_nick_doc <= 2
              notifier.ping "Caught exception; exiting #{err}"
              retry
            end
          end

          nickname_geolocation = Uri.nickname_geolocation(nick_doc)
          existing_nickname = Nickname.find_by(name: nickname)

          unless existing_nickname
            new_nickname = Nickname.create(name: nickname,
                                           geolocation: nickname_geolocation)
          end

          if new_nickname
            NicknameParseReason.create(nickname_id: new_nickname.id,
                                       parse_reason_id: parse_reason.id)
          else
            NicknameParseReason.create(nickname_id: existing_nickname.id,
                                       parse_reason_id: parse_reason.id)
          end
        end

        if button_status == "Next"
          break
        else
          if link_name == "Next"
            base_uri = next_page
            Link.create(uri: next_page,
                        parse_reason_id: parse_reason.id) unless next_page == ""
          else
            base_uri = next_page2
            Link.create(uri: next_page2, parse_reason_id: parse_reason.id)
          end
        end

        sleep 240
      else
        $i = 0
        base_uri = next_page
        next
      end
    end
  end
end
