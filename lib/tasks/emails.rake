namespace :emails do
  desc "get all emails from nicknames"
  task take_emails: :environment do
    REGEXP_EMAIL = /\A[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}\z/i

    Nickname.in_batches(of: 15).each do |nicknames|
      nicknames.each do |nick|
        nick_last = Nickname.last.id
        output = %x[github-email #{nick[:name]}]
        data = (output.gsub /[\e\n\t]/, " ").split
        selected = data.select { |emails| emails =~ REGEXP_EMAIL }
        selected.uniq.map { |email| Email.create(email: email,
                                                 nickname_id: nick[:id]) }

        break if nick[:id] == nick_last
      end
      sleep 960
    end
  end
end
