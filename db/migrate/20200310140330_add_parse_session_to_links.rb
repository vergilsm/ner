class AddParseSessionToLinks < ActiveRecord::Migration[5.2]
  def change
    add_reference :links, :parse_session, index: true, foreign_key: true
  end
end
