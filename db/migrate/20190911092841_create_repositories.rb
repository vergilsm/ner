class CreateRepositories < ActiveRecord::Migration[5.2]
  def change
    create_table :repositories do |t|
      t.string :name
      t.string :star
      t.string :commit
      t.references :nickname, index: true, foreign_key: true

      t.timestamps
    end
  end
end
