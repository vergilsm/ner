class DropGeolocations < ActiveRecord::Migration[5.2]
  def change
    drop_table :geolocations do |t|
      t.string :user_geolocation
      t.references :nickname, index: true, foreign_key: true

      t.timestamps
    end
  end
end
