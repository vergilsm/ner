class CreateRepositoryParseReasons < ActiveRecord::Migration[5.2]
  def change
    create_table :repository_parse_reasons do |t|
      t.references :repository, index: true, foreign_key: true
      t.references :parse_reason, index: true, foreign_key: true

      t.timestamps
    end
  end
end
