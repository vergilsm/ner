class CreateParseReasons < ActiveRecord::Migration[5.2]
  def change
    create_table :parse_reasons do |t|
      t.string :title

      t.timestamps
    end
  end
end
