class CreateParseSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :parse_sessions do |t|
      t.references :parse_reason, index: true, foreign_key: true

      t.timestamps
    end
  end
end
