class RemoveNicknameGeolocationFromNicknames < ActiveRecord::Migration[5.2]
  def change
    remove_column :nicknames, :nickname_geolocation, :string
  end
end
