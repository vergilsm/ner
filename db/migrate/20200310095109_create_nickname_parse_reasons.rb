class CreateNicknameParseReasons < ActiveRecord::Migration[5.2]
  def change
    create_table :nickname_parse_reasons do |t|
      t.references :nickname, index: true, foreign_key: true
      t.references :parse_reason, index: true, foreign_key: true

      t.timestamps
    end
  end
end
