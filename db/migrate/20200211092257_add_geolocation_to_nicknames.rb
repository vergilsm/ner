class AddGeolocationToNicknames < ActiveRecord::Migration[5.2]
  def change
    add_column :nicknames, :geolocation, :string
  end
end
