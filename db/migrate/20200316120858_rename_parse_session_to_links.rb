class RenameParseSessionToLinks < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :links, :parse_sessions
    rename_column :links, :parse_session_id, :parse_reason_id
  end
end
