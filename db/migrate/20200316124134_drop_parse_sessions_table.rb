class DropParseSessionsTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :parse_sessions do |t|
      t.references :parse_reason, index: true, foreign_key: true

      t.timestamps
    end
  end
end
