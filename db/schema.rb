# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_16_124134) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "emails", force: :cascade do |t|
    t.string "email"
    t.bigint "nickname_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nickname_id"], name: "index_emails_on_nickname_id"
  end

  create_table "links", force: :cascade do |t|
    t.string "uri"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "parse_reason_id"
    t.index ["parse_reason_id"], name: "index_links_on_parse_reason_id"
  end

  create_table "nickname_parse_reasons", force: :cascade do |t|
    t.bigint "nickname_id"
    t.bigint "parse_reason_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nickname_id"], name: "index_nickname_parse_reasons_on_nickname_id"
    t.index ["parse_reason_id"], name: "index_nickname_parse_reasons_on_parse_reason_id"
  end

  create_table "nicknames", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "geolocation"
  end

  create_table "parse_reasons", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "repositories", force: :cascade do |t|
    t.string "name"
    t.string "star"
    t.string "commit"
    t.bigint "nickname_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nickname_id"], name: "index_repositories_on_nickname_id"
  end

  create_table "repository_parse_reasons", force: :cascade do |t|
    t.bigint "repository_id"
    t.bigint "parse_reason_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parse_reason_id"], name: "index_repository_parse_reasons_on_parse_reason_id"
    t.index ["repository_id"], name: "index_repository_parse_reasons_on_repository_id"
  end

  add_foreign_key "emails", "nicknames"
  add_foreign_key "nickname_parse_reasons", "nicknames"
  add_foreign_key "nickname_parse_reasons", "parse_reasons"
  add_foreign_key "repositories", "nicknames"
  add_foreign_key "repository_parse_reasons", "parse_reasons"
  add_foreign_key "repository_parse_reasons", "repositories"
end
