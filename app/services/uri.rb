class Uri
  def self.base_uri(parse_reason_title)
    case parse_reason_title
    when "have repository"
      "https://github.com/rails/rails/network/dependents"
    when "liked rails"
      "https://github.com/rails/rails/stargazers"
    end
  end

  def self.user_agent
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) " \
                 "AppleWebKit/535.2 (KHTML, like Gecko) " \
                 "Chrome/15.0.854.0 Safari/535.2"
  end

  def self.doc(base_uri, user_agent)
    Nokogiri::HTML(open(base_uri,'User-Agent' => user_agent,
                        'read_timeout' => '10' ), nil, "UTF-8")
  end


  def self.link_name(doc)
    doc.xpath("//div[@class='paginate-container']/div/a[2]").text
  end

  def self.next_page(doc)
    doc.xpath("//div[@class='paginate-container']/div/a[2]/@href").text
  end

  def self.next_page2(doc)
    doc.xpath("//div[@class='paginate-container']/div/a/@href").text
  end

  def self.button_status(doc)
    doc.xpath("//div[@class='paginate-container']//button").text
  end

  def self.repositories_on_page(doc)
    doc.xpath("//div[@class='Box']//div[@class='Box-row d-flex " \
              "flex-items-center']/span/a[2]/@href[1]").map { |x| x.to_s }
  end

  def self.repo_url(repo)
    "https://github.com#{repo}"
  end

  def self.repo_doc(repo_url, user_agent)
    Nokogiri::HTML(open(repo_url,'User-Agent' => user_agent,
                        'read_timeout' => '10' ), nil, "UTF-8")
  end

  def self.nickname(repo_doc)
    repo_doc.xpath("//div[@class='flex-auto min-width-0 width-fit mr-3']/" \
                   "h1/span/a").text
  end

  def self.nick_uri(nickname)
    "https://github.com/#{nickname}"
  end

  def self.nick_doc(nick_uri, user_agent)
    Nokogiri::HTML(open(nick_uri,'User-Agent' => user_agent,
                        'read_timeout' => '10' ), nil, "UTF-8")
  end

  def self.nickname_geolocation(nick_doc)
    nickname_geolocation = nick_doc.xpath("//div/ul/li/span" \
                                          "[@itemprop='location']")
    nickname_geolocation2 = nick_doc.xpath("//div/ul/li/span" \
                                           "[@class='p-label']/text()").last

    unless nickname_geolocation2.nil? || nickname_geolocation2.text.empty?
      nick_geolocation = nickname_geolocation2.text
    end

    unless nickname_geolocation.nil? || nickname_geolocation.text.empty?
      nick_geolocation = nickname_geolocation.text
    end

    return nick_geolocation
  end

  def self.repo_name(repo_doc)
    repo_doc.xpath("//div[@class='flex-auto min-width-0 " \
                   "width-fit mr-3']/h1/strong/a").text
  end

  def self.star(repo_doc)
    repo_doc.xpath("//ul[@class='pagehead-actions flex-shrink-0']" \
                   "/li[2]//a").text.delete("Star").strip
  end

  def self.commit(repo_doc)
    repo_doc.xpath("//ul/li[@class='commits']/a/span").text.strip
  end

  # Stargazers
  def self.stargazers_on_page(doc)
    doc.xpath("//div[@class='repository-content ']//ol/li//div/h3" \
              "/span/a/@href").map { |x| x.to_s  }
  end
end
