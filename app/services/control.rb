class Control
  def self.logger
    Logger.new("log/repos_nicknames.log")
  end

  def self.notifier
    Slack::Notifier.new Rails.application.credentials.slack_message_uri do
       defaults channel: "#ner"
     end
  end
end
