class NicknameParseReason < ApplicationRecord
  belongs_to :nickname
  belongs_to :parse_reason
end
