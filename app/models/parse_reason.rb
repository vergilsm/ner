class ParseReason < ApplicationRecord
  has_many :nicknames, through: :nickname_parse_reasons
  has_many :repositories, through: :nickname_parse_reasons
  has_many :links

  validates :title, presence: true, uniqueness: true
end
