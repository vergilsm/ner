class Email < ApplicationRecord
  belongs_to :nickname

  validates :email, presence: true, uniqueness: true
  validates :nickname_id, presence: true
end
