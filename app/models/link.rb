class Link < ApplicationRecord
  belongs_to :parse_reason

  validates :uri, :parse_reason_id, presence: true
end
