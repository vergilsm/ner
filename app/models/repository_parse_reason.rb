class RepositoryParseReason < ApplicationRecord
  belongs_to :repository
  belongs_to :parse_reason
end
