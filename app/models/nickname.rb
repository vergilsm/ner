class Nickname < ApplicationRecord
  has_many :repositories
  has_many :emails
  has_many :parse_reasons, through: :nickname_parse_reasons

  validates :name, presence: true, uniqueness: true
end
