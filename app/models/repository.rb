class Repository < ApplicationRecord
  belongs_to :nickname
  has_many :parse_reasons, through: :repository_parse_reasons

  validates :name, presence: true
  validates :nickname_id, presence: true
end
